.release_docker_images:
  extends:
  - .docker
  - .docker_qemu
  - .linux-dependency-checksums
  stage: release
  variables:
    PUBLISH_IMAGES: "true"
    PUSH_TO_DOCKER_HUB: "true"
    PUSH_TO_ECR_PUBLIC: "true"
    TARGET_ARCHS: "amd64,arm64,s390x"
  dependencies:
  - 'helper images'
  - 'ecr public token'
  - 'binaries linux/386 linux/amd64 linux/arm linux/arm64 linux/s390x'
  - 'package-deb'
  script:
  - source ci/touch_make_dependencies
  - make release_docker_images
  retry: 2

.release_windows_docker_images:
  extends:
  - .windows-dependency-checksums
  stage: release
  variables:
    PUSH_TO_DOCKER_HUB: "true"
    PUSH_TO_ECR_PUBLIC: "true"
    PUBLISH_IMAGES: "true"
  dependencies:
  - 'helper images'
  - 'ecr public token'
  - 'binaries windows/386 windows/amd64'
  script:
  - .\ci\build_release_windows_images.ps1
  retry: 2

.release:
  stage: release
  before_script:
  - source ci/touch_make_dependencies

.release_s3:
  extends:
  - .release
  dependencies:
  - 'helper images'
  - 'test coverage report'
  - 'binaries darwin/amd64'
  - 'binaries freebsd/386 freebsd/amd64 freebsd/arm'
  - 'binaries linux/386 linux/amd64 linux/arm linux/arm64 linux/s390x'
  - 'binaries windows/386 windows/amd64'
  - 'package-deb'
  - 'package-rpm'
  before_script:
  - source ci/touch_make_dependencies
  - |
    # checking GPG signing support
    if [ -f "$GPG_KEY_PATH" ]; then
      export GPG_KEY=$(cat ${GPG_KEY_PATH})
      export GPG_PASSPHRASE=$(cat ${GPG_PASSPHRASE_PATH})
    else
      echo -e "\033[0;31m****** GPG signing disabled ******\033[0m"
    fi
  script:
  - make release_s3

.release_packagecloud:
  extends:
  - .release
  dependencies:
  - 'package-deb'
  - 'package-rpm'
  script:
  - make release_packagecloud

development S3:
  extends:
  - .release_s3
  - .rules:runner-only:release:development:merge-requests
  environment:
    name: development/s3/${CI_COMMIT_REF_NAME}
    url: https://gitlab-runner-downloads.s3.amazonaws.com/${CI_COMMIT_REF_NAME}/index.html

development docker images:
  extends:
  - .release_docker_images
  - .rules:release:development:merge-requests
  variables:
    PUBLISH_IMAGES: "false"
    PUSH_TO_DOCKER_HUB: "false"
    PUSH_TO_ECR_PUBLIC: "false"

development servercore1809 helper docker image:
  extends:
  - .windows1809
  - .release_windows_docker_images
  - .rules:release:development:merge-requests
  variables:
    PUBLISH_IMAGES: "false"
    PUSH_TO_DOCKER_HUB: "false"
    PUSH_TO_ECR_PUBLIC: "false"

development servercore2004 helper docker image:
  extends:
  - .windows2004
  - .release_windows_docker_images
  - .rules:release:development:merge-requests
  variables:
    PUBLISH_IMAGES: "false"
    PUSH_TO_DOCKER_HUB: "false"
    PUSH_TO_ECR_PUBLIC: "false"

development servercore20H2 helper docker image:
  extends:
  - .windows20H2
  - .release_windows_docker_images
  - .rules:release:development:merge-requests
  variables:
    PUBLISH_IMAGES: "false"
    PUSH_TO_DOCKER_HUB: "false"
    PUSH_TO_ECR_PUBLIC: "false"

bleeding edge S3:
  extends:
  - .release_s3
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/s3
    url: https://gitlab-runner-downloads.s3.amazonaws.com/${CI_COMMIT_REF_NAME}/index.html

bleeding edge packagecloud:
  extends:
  - .release_packagecloud
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/packagecloud
    url: https://packages.gitlab.com/runner/unstable

bleeding edge docker images:
  extends:
  - .release_docker_images
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/docker_images/linux
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

bleeding edge servercore1809 docker images:
  extends:
  - .windows1809
  - .release_windows_docker_images
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/docker_images/windows1809
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

bleeding edge servercore2004 docker images:
  extends:
  - .windows2004
  - .release_windows_docker_images
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/docker_images/windows2004
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

bleeding edge servercore20H2 docker images:
  extends:
  - .windows20H2
  - .release_windows_docker_images
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/docker_images/windows20H2
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

stable S3:
  extends:
  - .release_s3
  - .rules:release:stable:branch
  environment:
    name: stable/s3
    url: https://gitlab-runner-downloads.s3.amazonaws.com/${CI_COMMIT_REF_NAME}/index.html

stable gitlab release:
  extends:
  - .release
  - .rules:release:stable-or-rc
  environment:
    name: stable/gitlab
    url: https://gitlab.com/gitlab-org/gitlab-runner/-/releases
  script:
  - ./ci/release_gitlab

stable packagecloud:
  extends:
  - .release_packagecloud
  - .rules:release:stable:branch
  environment:
    name: stable/packagecloud
    url: https://packages.gitlab.com/runner/gitlab-runner

stable docker images:
  extends:
  - .release_docker_images
  - .rules:release:stable:branch
  environment:
    name: stable/docker_images/linux
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

stable servercore1809 docker images:
  extends:
  - .windows1809
  - .release_windows_docker_images
  - .rules:release:stable:branch
  environment:
    name: stable/docker_images/windows1809
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

stable servercore2004 docker images:
  extends:
  - .windows2004
  - .release_windows_docker_images
  - .rules:release:stable:branch
  environment:
    name: stable/docker_images/windows2004
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

stable servercore20H2 docker images:
  extends:
  - .windows20H2
  - .release_windows_docker_images
  - .rules:release:stable:branch
  environment:
    name: stable/docker_images/windows20H2
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

static QA:
  extends:
  - .rules:merge_request_pipelines:no_docs
  - .no_cache
  stage: release
  image: alpine:3.12.0
  needs:
  - code_quality
  script: |
    if [ "$(cat gl-code-quality-report.json)" != "[]" ] ; then
      apk add -U --no-cache jq > /dev/null
      jq -C . gl-code-quality-report.json
      exit 1
    fi
